import csv
import os
from scipy import interpolate
import numpy as np
import matplotlib.pyplot as plt
import sys

def read_data (infile):
    empid = []
    ratio = []
    score = []
    salary = []
    assert (os.path.isfile(infile))
    firstline=True
    with open (infile,'r') as fp:
        reader = csv.reader (fp, delimiter=';')
        for row in reader:
            if firstline:
                firstline=False
                continue
            d = [float(a.replace(',','.')) for a in row]
            empid.append(int(d[0]))
            salary.append (d[1])
            score.append (d[2])
            ratio.append (d[3])
    return (empid, ratio, score, salary)


def get_limits ():
    x = np.array ([0.8, 0.85, 1.0, 1.15, 1.2]) # comp ratios
    y = np.array ([4.5, 3.75, 3.35, 3.05, 2.9]) # scores
    # load data locally if not available on share drive
    raise_file = '/media/sf_linux-share/raises.txt'
    if not os.path.isfile (raise_file):
        raise_file = 'raises.txt'
    z = np.loadtxt (raise_file, delimiter=';') # raises

    # flip for display
    y = y[::-1]
    print (y)
    z = np.flipud(z)
    print (z)
    f = interpolate.interp2d(x, y, z)

    return f

    znew = f(x,y)
    print (znew.shape)
    print(znew)
    print ('check')
    print (f(0.8,4.3))
    plt.contourf (x,y,znew,cmap='RdBu')
    plt.xlabel('Comp ratio')
    plt.ylabel('Score')
    plt.title('Raise estimation')
    plt.colorbar()
    plt.show()
    return f


def help_ ():
    print ("Usage : python run.py <infile.csv> <outfile.csv>")
    print ("")

def main():
    if len(sys.argv)<3:
        help_()
        return

    input_file = sys.argv[1]
    output_file = sys.argv[2]

    f = get_limits ()
    
    (empid_v, ratio_v, score_v, salary_v) = read_data (input_file)
    maxraise=0
    avgraise=0
    budget=0
    nel=0
    with open (output_file,'w') as outp:
        for empid, ratio, score, salary in zip (empid_v, ratio_v, score_v, salary_v):
            raised = f(ratio,score)[0]
            print ('ID = %d, score = %6.1f, ratio = %6.2f, raise = %6.5f ==> new salary = %.1f' % (empid, score, ratio, raised, salary * (1.0+raised/100)))
            maxraise=max([maxraise,raised])
            avgraise+=raised
            budget += salary * raised/100
            outp.write ("%d;%s\n" % (empid, str(int(100*raised)/100.0).replace('.',',')))
            nel+=1
    if nel>0:
        avgraise /= nel

    print ("Max raise : %.2f%%" % (1.0*maxraise))
    print ("Avg raise : %.2f%%" % (1.0*avgraise))
    print ("Budget : %d" % int(budget))

if __name__ == "__main__":
    main()
